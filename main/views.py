from django.shortcuts import render
from rdflib import Graph, Literal, RDF, URIRef
from SPARQLWrapper import SPARQLWrapper

# Mock Up dict
response = {
    'photo': "https://cdn.sofifa.com/players/192/985/19_120.png",
    'name': "Kevin De Bruyne",
    'club': "Manchester City",
    'nationality': "Belgium",
    'age': "27",
    'score': "91",
    'number': "17",
    'height': "5'11",
    'weight': "154lbs"
}

def name_extract(team):
    teams = team.split("|")
    result = []
    
    for team in teams:
        result.append(team.replace(" ", "_")) #exchange space with underscore
    
    return result

def name_dextract(name):
    return name.replace("_", " ") #return original naming styles

def player_urifier(player):
    player = player.replace(" ", "_") #exchange space with underscore

    result = "http://example.org/FIFA19/Players/" + player

    return result

# Query from local

def local_query(player, data):
    g = Graph()
    g.parse("fifa19mini.ttl", format="ttl")

    player_queried = player_urifier(player)

    query = """
    PREFIX dbp: <http://dbpedia.org/property/>
    PREFIX dbo: <http://dbpedia.org/ontology/>
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX ns1: <http://dbpedia.org/ontology/>
    PREFIX ns2: <http://xmlns.com/foaf/0.1/>

    SELECT DISTINCT (group_concat(distinct ?images ; separator="|") as ?photos) ?name (group_concat(distinct ?team_name ; separator="|") as ?team) ?age ?score ?number ?weight ?height
    WHERE {{
      ?player a ns1:SoccerPlayer ;
                ns2:name ?name ;
                ns1:age ?age ;
                ns1:score ?score ;
                ns1:number ?number ;
                ns1:weight ?weight ;
                ns1:height ?height ;
                ns2:logo ?images ;
                ns1:team ?team_name .
      FILTER regex(str(?player), "%s") .
      }}
    GROUP BY ?name ?age ?score ?number ?weight ?height
    """% player_queried

    query_result = g.query(query)
    result = make_dictionary(data, query_result)

    return dbpedia_query(result)
    

def make_dictionary(data, query_result):
    for row in query_result:        
        #take player picture
        photos = row[0].split("|")
        photo = ""
        for pic in photos:
            if "player" in pic:
                photo = pic

        data["photo"] += photo
        data["name"] += row[1]
        data["age"] += row[3]
        data["score"] += row[4]
        data["number"] += row[5]
        data["height"] += row[6]
        data["weight"] += row[7]
        data["team"] += row[2]
  
    return data

# Query from DBPedia

def dbpedia_query(data):

    team_name = name_extract(data["team"])

    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery("""
    PREFIX dbo: <http://dbpedia.org/ontology/>
    PREFIX dbp: <http://dbpedia.org/property/>
    PREFIX dbr: <http://dbpedia.org/resource/>
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

    SELECT ?name
    WHERE {{
        dbr:{0} a dbo:Location ;
                  dbp:commonName ?name .
    }}""".format(team_name[0]))

    sparql.setReturnFormat('json')
    result = sparql.query().convert()

    try:
        temp = result["results"]["bindings"][0]
        data["club"] += name_dextract(team_name[1])
        data["nationality"] += name_dextract(team_name[0])
    except:
        data["club"] += name_dextract(team_name[0])
        data["nationality"] += name_dextract(team_name[1])

    return data

def index(request):
    return render(request, 'templates.html', response)

def search(request):
    if request.method == "POST":
        result = {
            'photo': "",
            'name': "",
            'club': "",
            'nationality': "",
            'age': "",
            'score': "",
            'number': "",
            'height': "",
            'weight': "",
            'team': ""
            }
        result = local_query(request.POST["search"], result)
        return render(request, 'templates.html', result)